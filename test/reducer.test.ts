import { assert, expect, test } from "vitest";
import { Status } from "../src/domain/entity/task";
import reducer, {
    insertTask,
    transactionTask,
} from "../src/domain/tasklistSlice";

test("Test insert Task", () => {
    const mockTask = { id: "3", title: "Test Data", status: Status.BACKOLOG };

    var initState = { content: [] };

    let state = reducer(initState, insertTask(mockTask));
    expect(state.content.length).toBe(1);

    state = reducer(state, insertTask(mockTask));
    expect(state.content.length).toBe(2);
});

test("Test transaction task", () => {
    const mockTask = { id: "3", title: "Test Data", status: Status.BACKOLOG };

    var initState = { content: [mockTask] };
    let state;

    state = reducer(
        initState,
        transactionTask({ taskId: "3", direction: true })
    );
    expect(state.content[0].status).toBe(Status.TODO);

    state = reducer(state, transactionTask({ taskId: "3", direction: true }));
    expect(state.content[0].status).toBe(Status.IN_PROGRESS);

    state = reducer(state, transactionTask({ taskId: "3", direction: true }));
    expect(state.content[0].status).toBe(Status.UNDER_REVIEW);

    state = reducer(state, transactionTask({ taskId: "3", direction: false }));
    expect(state.content[0].status).toBe(Status.IN_PROGRESS);

    state = reducer(state, transactionTask({ taskId: "3", direction: true }));
    expect(state.content[0].status).toBe(Status.UNDER_REVIEW);

    state = reducer(state, transactionTask({ taskId: "3", direction: true }));
    expect(state.content[0].status).toBe(Status.DONE);

    state = reducer(state, transactionTask({ taskId: "3", direction: true }));
    expect(state.content[0].status).toBe(Status.CLOSED);
});

test("Teest mark Start and Finish Time", () => {
    const mockTask = { id: "3", title: "Test Data", status: Status.BACKOLOG };

    var initState = { content: [mockTask] };
    let state;

    // BACKLOG -> TODO
    state = reducer(
        initState,
        transactionTask({ taskId: "3", direction: true })
    );
    expect(state.content[0].startTime).toBeUndefined;
    expect(state.content[0].endTime).toBeUndefined;

    // TODO -> IN_PROGRESS with start time
    state = reducer(state, transactionTask({ taskId: "3", direction: true }));
    expect(state.content[0].startTime).toBeInstanceOf(Date);
    expect(state.content[0].endTime).toBeUndefined;

    // IN_PROGRESS -> UNDER_REVIEW with start time
    state = reducer(state, transactionTask({ taskId: "3", direction: true }));
    expect(state.content[0].startTime).toBeInstanceOf(Date);
    expect(state.content[0].endTime).toBeUndefined;

    // UNDER_REVIEW -> DONE with start time
    state = reducer(state, transactionTask({ taskId: "3", direction: true }));
    expect(state.content[0].startTime).toBeInstanceOf(Date);
    expect(state.content[0].endTime).toBeInstanceOf(Date);
});
