import { Task } from "../domain/entity/task";

export function getStoredTasks(): Array<Task> {
    return mockData;
}

export function saveTasks(data: Array<Task>) {}

const mockData: Array<Task> = [
    // { id: "1", title: "Sample", status: Status.BACKOLOG },
    // { id: "2", title: "another sample", status: Status.BACKOLOG },
    // { id: "3", title: "Test Data", status: Status.BACKOLOG },
    // {
    //     id: "4",
    //     title: "Some Story",
    //     content: "With content",
    //     status: Status.IN_PROGRESS,
    //     startTime: new Date(),
    //     endTime: new Date(),
    //     assignee: {
    //         uid: "a1",
    //         name: "Mike",
    //     },
    // },
    // {
    //     id: "5",
    //     title: "Try new stuff",
    //     status: Status.IN_PROGRESS,
    //     startTime: new Date(),
    //     assignee: {
    //         uid: "a2",
    //         name: "Wang",
    //     },
    // },
];
