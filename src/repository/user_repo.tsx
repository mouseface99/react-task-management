import { User } from "../domain/entity/user";

export function getUsers(): Array<User> {
    return userList;
}

const userList = [
    {
        uid: 'a1',
        name: 'Mike',
    },
    {
        uid: 'a2',
        name: 'John',
    },
    {
        uid: 'a3',
        name: 'Rose',
    },
    {
        uid: 'a4',
        name: 'Yelp',
    }
]