import { Status } from "../domain/entity/task";
import { useTaskStore } from "../domain/store";
import "./board.css";
import { Column } from "./views/column";

export function TaskBoard() {
    const list = useTaskStore((state) => state.content);

    const backlog = list.filter((task) => task.status == Status.BACKOLOG);
    const todo = list.filter((task) => task.status == Status.TODO);
    const progress = list.filter((task) => task.status == Status.IN_PROGRESS);
    const review = list.filter((task) => task.status == Status.UNDER_REVIEW);
    const done = list.filter((task) => task.status == Status.DONE);

    return (
        <div>
            <div className="column red">
                <h1 className="header">Backlog ({backlog.length})</h1>
                <Column data={backlog} />
            </div>
            <div className="column yellow">
                <h1 className="header">To Do ({todo.length})</h1>
                <Column data={todo} />
            </div>
            <div className="column blue">
                <h1 className="header">In Progress ({progress.length})</h1>
                <Column data={progress} />
            </div>
            <div className="column brown">
                <h1 className="header">Under Review ({review.length})</h1>
                <Column data={review} />
            </div>
            <div className="column green">
                <h1 className="header">Done ({done.length})</h1>
                <Column data={done} />
            </div>
        </div>
    );
}
