import axios from "axios";
import { FormEvent } from "react";
import { useRef, useState } from "react";
import { Status, Task } from "../domain/entity/task";
import { User } from "../domain/entity/user";
import { useTaskStore } from "../domain/store";
import { getUsers } from "../repository/user_repo";
import "./board.css";

export function ControlPanel() {
    const users = getUsers();
    const titleRef = useRef<HTMLInputElement>(null);
    const contentRef = useRef<HTMLInputElement>(null);
    const [assignee, setAssignee] = useState<User>();

    const insertTask = useTaskStore((state) => state.insertTask);
    const resetTask = useTaskStore((state) => state.reset);

    const handleSubmit = async (e: FormEvent) => {
        e.preventDefault();
        if (!titleRef.current?.value) {
            alert("Title is Required");
            return;
        }

        const id = Math.random() * 10000;
        const image = await axios.get("https://random.dog/woof.json");

        const newTask: Task = {
            id: id.toFixed().toString(),
            title: titleRef.current!.value,
            content: contentRef.current?.value,
            status: Status.BACKOLOG,
            assignee: assignee,
            imageUrl: image.data["url"],
        };

        insertTask(newTask);

        titleRef.current.value = "";
        if (contentRef.current) {
            contentRef.current.value = "";
        }
    };

    return (
        <div className="panel">
            <h1>TODO Board</h1>
            <div>
                <button onClick={resetTask}>Reset</button>
            </div>
            <div className="inputForm">
                <form onSubmit={handleSubmit} id="inputForm">
                    <h3>Create New Task</h3>
                    <p>
                        <label htmlFor="title">Title:</label>
                        &emsp;&emsp;&emsp;
                        <input
                            name="title"
                            ref={titleRef}
                            value={titleRef.current?.value}
                        />
                    </p>
                    <p>
                        <label htmlFor="content">Content(Optional):</label>
                        &emsp;&emsp;&emsp;
                        <input
                            name="content"
                            ref={contentRef}
                            value={contentRef.current?.value}
                        />
                    </p>
                    <p>
                        <label htmlFor="assignee">Assignee</label>
                        &emsp;&emsp;&emsp;
                        <select
                            name="assignee"
                            onChange={(e) => {
                                const user = users?.find(
                                    (user) => user.uid === e.target.value
                                );
                                setAssignee(user);
                            }}
                        >
                            <option>Unassign</option>
                            {users?.map((user: User) => (
                                <option value={user.uid} key={user.uid}>
                                    {user.name}
                                </option>
                            ))}
                        </select>
                    </p>
                    <button type="submit">Create</button>
                </form>
            </div>
        </div>
    );
}
