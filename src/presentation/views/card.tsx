import "./card.css";
import { Status, Task } from "../../domain/entity/task";
import dateFormat from "dateformat";
import { useTaskStore } from "../../domain/store";

export interface CardProp {
    data: Task;
}

export function Card(prop: CardProp) {
    const data = prop.data;
    return (
        <div className="card">
            <div className="center-cropped">
                <img src={data.imageUrl} alt="" />
            </div>
            <div className="row">
                <div className="field">Title:</div>
                <div className="field">
                    <b>{data.title}</b>
                </div>
            </div>
            {data.content ? (
                <div className="row">
                    <div className="field">Content: </div>
                    <div className="field">{data.content}</div>
                </div>
            ) : null}
            {data.assignee ? (
                <div className="row author">
                    <div className="field">Assignee: </div>
                    <div className="field">
                        @{data.assignee ? data.assignee.name : "Unknown"}
                    </div>
                </div>
            ) : null}
            {data.startTime ? (
                <div className="row">
                    <div className="field">Starts at:</div>
                    <div className="date field">{format(data.startTime)}</div>
                </div>
            ) : null}
            {data.endTime ? (
                <div className="row">
                    <div className="field">End at:</div>
                    <div className="date field">{format(data.endTime)}</div>
                </div>
            ) : null}
            <ButtonSection data={prop.data} />
        </div>
    );
}

function format(time: Date): string {
    return dateFormat(time, "mm/dd HH:MM:ss");
}

function ButtonSection(prop: CardProp) {
    const transactionTask = useTaskStore((state) => state.transactionTask);

    const data = prop.data;
    const forward = () => transactionTask(data.id, true);
    const back = () => transactionTask(data.id, false);

    let left, center, right;

    right = button("Next", forward);
    left = button("Back", back);

    switch (data.status) {
        case Status.BACKOLOG:
            left = undefined;
            break;
        case Status.DONE:
            center = button("Close", forward);
            left = undefined;
            right = undefined;
            break;
    }

    return (
        <div className="row">
            <div className="field">{left}</div>
            <div className="field">{center}</div>
            <div className="field">{right}</div>
        </div>
    );
}

function button(text: string, onclick: () => void) {
    return <button onClick={onclick}>{text}</button>;
}
