import { Task } from "../../domain/entity/task";
import { Card } from "./card";

export interface TaskListProp {
    data?: Array<Task>;
}

export function Column(prop: TaskListProp) {
    const { data } = prop;
    if (!data || !data.length) {
        return (
            <div style={{ padding: "16px" }}>
                <h3>No users found</h3>
            </div>
        );
    }
    return (
        <div>
            {data.map((task) => (
                <Card key={task.id} data={task} />
            ))}
        </div>
    );
}
