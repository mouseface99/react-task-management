import { User } from "./user";

export interface Task {
    id: string;
    title: string;
    content?: string;
    assignee?: User;
    status: Status;
    startTime?: Date;
    endTime?: Date;
    imageUrl: string;
}

export enum Status {
    BACKOLOG,
    TODO,
    IN_PROGRESS,
    UNDER_REVIEW,
    DONE,
    CLOSED,
}

export function transState(origStatus: Status, direction: boolean): Status {
    switch (origStatus) {
        case Status.BACKOLOG:
            return Status.TODO;
        case Status.TODO:
            return direction ? Status.IN_PROGRESS : Status.BACKOLOG;
        case Status.IN_PROGRESS:
            return direction ? Status.UNDER_REVIEW : Status.TODO;
        case Status.UNDER_REVIEW:
            return direction ? Status.DONE : Status.IN_PROGRESS;
        case Status.DONE:
        case Status.CLOSED:
            return Status.CLOSED;
    }
}
