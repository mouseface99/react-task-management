import create from "zustand";
import { devtools, persist } from "zustand/middleware";
import { getStoredTasks } from "../repository/task_repo";
import { Status, Task, transState } from "./entity/task";

interface TaskState {
    content: Array<Task>;
    insertTask: (data: Task) => void;
    transactionTask: (taskId: string, direction: boolean) => void;
    reset: () => void;
}

export const useTaskStore = create<TaskState>()(
    devtools(
        persist((set) => ({
            content: getStoredTasks(),
            insertTask: (data) =>
                set((state) => {
                    const list = [...state.content];
                    list.push(data);
                    return { content: list };
                }),
            transactionTask: (taskId, direction) =>
                set((state) => {
                    const list = [...state.content];
                    const target = list.find(
                        (task: Task) => task.id === taskId
                    );
                    if (target) {
                        const nextStatus = transState(
                            target?.status,
                            direction
                        );
                        target.status = nextStatus;
                        if (nextStatus == Status.IN_PROGRESS) {
                            target.startTime = new Date();
                        }
                        if (nextStatus == Status.DONE) {
                            target.endTime = new Date();
                        }

                        return { content: list };
                    }
                    return state;
                }),
            reset: () =>
                set((state) => {
                    return { content: [] };
                }),
        })),
        {
            name: "task-storage",
        }
    )
);
