import "./App.css";
import { ControlPanel } from "./presentation/control_panel";
import { TaskBoard } from "./presentation/task_board";

function App() {
    return (
        <div>
            <ControlPanel />
            <TaskBoard />
        </div>
    );
}

export default App;
